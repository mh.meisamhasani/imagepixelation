// QuadTree.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <fstream>

#include <opencv2/ml/ml.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

struct Matr
{
	Mat img;
	Rect coor;
};

int main(int argc, char ** argv)
{
	if (argc < 1){ std::cout << "Not enough inputs (Example Command Run: \"./ImagePixelation someimagename.jpg 15\" or  \"./ImagePixelation  someimagename.jpg 5.08\" )\n";
return 0;
}

	if (argc > 3) { std::cout << "Only two arguments is needed (Example Command Run: \"./ImagePixelation someimagename.jpg 15\" or  \"./ImagePixelation  someimagename.jpg 5.08\" )\n";
return 0;
}

	//float MeanOfStd = 15; // Compression parameter. The more MeanOfStd, the more compression (lower qulity of the result image).
	float MeanOfStd = (float)strtod(argv[2],NULL);

	if (MeanOfStd <= 0){  std::cout << "the std number must be bigger than 0\n";
return 0;
}

	std::cout <<argc<<" "<<argv[0]<<" "<<argv[1]<<" "<<argv[2]<<"\n";

	Mat im = imread(argv[1],1);	

	Mat resM = Mat::zeros(im.size(), CV_32FC(im.channels()));

	vector<Matr> Qu;

	copyMakeBorder(im, im, 0, im.cols % 2, 0, im.rows % 2, BORDER_REFLECT);

	Matr image;
	image.img = im;
	image.coor = Rect(0, 0, im.cols, im.rows);

	Qu.push_back(image);
	int I = 0;
	
	int row_1, row_2, col_1, col_2;
	float mean_of_std;

	while ( I < Qu.size())
	{
		vector<Rect> rs;
		Mat img = Qu[I].img;
		Rect coor = Qu[I].coor;

		row_1 = img.rows / 2;
		row_2 = img.rows - row_1;
		col_1 = img.cols / 2;
		col_2 = img.cols - col_1;
		
		rs.push_back(Rect(0, 0, col_1, row_1));
		rs.push_back(Rect(col_1, 0, col_2, row_1));
		rs.push_back(Rect(0, row_1, col_1, row_2));
		rs.push_back(Rect(col_1, row_1, col_2, row_2));

		for (size_t i = 0; i < 4; i++) // 4 iterations, because it's a "QUAD" tree!
		{
			Scalar mea, std;
			meanStdDev(img(rs[i]), mea, std);

			mean_of_std = 0;
			float k = 0;

			for (size_t t = 0; t < 4; t++) // 4 here is the Scalar array size
				if (std.val[t] > 0) { mean_of_std += std.val[t]; k++; }
			
			mean_of_std /= k;

			if (mean_of_std > MeanOfStd && rs[i].height > 2 && rs[i].width > 2)
			{
				Matr m;
				m.img = img(rs[i]);
				m.coor = Rect(coor.x + rs[i].x, coor.y + rs[i].y, rs[i].width, rs[i].height);
				Qu.push_back(m);
			}
			else
			{
				Rect rc(coor.x + rs[i].x, coor.y + rs[i].y, rs[i].width, rs[i].height);
				vector<Mat>chs;
				split(resM(rc), chs);
				for (size_t k = 0; k < im.channels(); k++)	chs[k].setTo(mea.val[k]);

				merge(chs, resM(rc));
			}
		}

		I++;
		img.release();
	}

	normalize(resM, resM, 0, 255, NORM_MINMAX);
	resM.convertTo(resM, CV_8U);

	imshow("1", im);
	imshow("2", resM);

	imwrite("Output_Image.jpg", resM);

	waitKey(0);	
	
	return 0;
}

