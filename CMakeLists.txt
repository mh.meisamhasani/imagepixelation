cmake_minimum_required(VERSION 3.2) 
PROJECT (ImagePixelation VERSION 1.0)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED True)

find_package(OpenCV REQUIRED )
set( NAME_SRC ImagePixelation.cpp)
#set( NAME_HEADERS header.h)

INCLUDE_DIRECTORIES( ${CMAKE_CURRENT_SOURCE_DIR}/include ${OpenCV_INCLUDE_DIRS} )

link_directories( ${CMAKE_BINARY_DIR}/bin)
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR})
add_executable( ImagePixelation ${NAME_SRC} ${NAME_HEADERS} )

target_link_libraries( ImagePixelation ${OpenCV_LIBS} )

set_property(TARGET ImagePixelation PROPERTY CXX_STANDARD 14)
set_property(TARGET ImagePixelation PROPERTY CXX_STANDARD_REQUIRED ON)
